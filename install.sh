#!/bin/bash
if [[ "$OSTYPE" == *"darwin"* ]]; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    brew update && brew upgrade python
    brew install pyenv
    python3 -m pip install virtualenv
fi

if [[ "$OSTYPE" == *"linux"* ]]; then
    if [[ "$(cat /etc/issue)" == *"Debian"* ]] || [[ "$(cat /etc/issue)" == *"Ubuntu"* ]]; then
        sudo apt-get install python3 python3-venv -y
    fi
    if [[ "$(cat /etc/issue)" == *"Arch"* ]]; then
        sudo pacman -S python python-virtualenv --needed --noconfirm
    fi
fi

root=$(echo "$(cd "$(dirname "$1")"; pwd -P)/$(basename "$1")")
python3 -m venv $root
cd $root
source $root/bin/activate
PATH=$root/bin:$PATH
$root/bin/python3 -m pip install --upgrade pip
$root/bin/python3 -m pip install -r $root/requirements/dev.txt
$root/bin/python3 setup.py bdist_wheel
$root/bin/python3 -m pip install -e $root