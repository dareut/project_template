import setuptools
from setuptools import setup, find_packages
from os import path
from io import open

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
        name='project_template',
        version='0.0.1',
        description='New python project - Template',
        long_description=long_description,
        long_description_content_type='text/markdown',
        url='https://gitlab.com/dareut/project_template',
        author='Daniel Reuter',
        author_email='daniel.reuter@atea.se',
        classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: Sun Industry Standards Source License (SISSL)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        ],
        keywords='python3 setuptools development template',
        package_dir={'': 'src'},
        packages=find_packages(where='src'),
        python_requires='!=3.0.*, !=3.1.*, !=3.2.*, !=3.3.*, !=3.4.*, <4',
        install_requires=['pandas'],
        project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/dareut/project_template/-/issues',
        'Authors Web': 'https://www.rojter.tech',
        'Source': 'https://gitlab.com/dareut/project_template',
        },
)