$root=$PSScriptRoot
$pyversion="3.7.6"
$pyurl="https://www.python.org/ftp/python/$pyversion/python-$pyversion-embed-amd64.zip"
$venvurl="https://bootstrap.pypa.io/virtualenv/3.7/virtualenv.pyz"

$targetdir="$root\env"
if (Test-Path $targetdir)
{
    $input = Read-Host -Prompt 'Found old binaries. Want to redownload a fresh copy? [y/N] (defalt: N)'
    if (($input) -like "y") 
    {
        Write-Output "Removing old binaries ..."
        Remove-Item $targetdir -Recurse -Force
    } 
    else { Write-Output "Keeping old binaries ..." }
}

$pytarget = "$targetdir\pybins.zip"
$venvtarget = "$targetdir\virtualenv.pyz"

if (-not (Test-Path $targetdir)) {
    mkdir $targetdir
    Invoke-WebRequest -Uri $pyurl -OutFile $pytarget
    Invoke-WebRequest -Uri $venvurl -OutFile $venvtarget
} else {
    if (-not (Test-Path $pytarget)){ Invoke-WebRequest -Uri $pyurl -OutFile $pytarget }
    if (-not (Test-Path $venvtarget)){ Invoke-WebRequest -Uri $venvurl -OutFile $venvtarget }
}

Expand-Archive -LiteralPath $pytarget -DestinationPath $targetdir -Force
$pybin="$targetdir\python.exe"
$dllspath="$targetdir\DLLs"
$targetfiles = Get-ChildItem -Path $targetdir -File | Select-Object -exp Name
if (-not (Test-Path $dllspath)) { mkdir $dllspath }
foreach ($filename in $targetfiles) {
    $filepath =  "$targetdir\$filename"
    $dllsfilepath = "$dllspath\$filename"
    if (($filename) -like "*.dll") { Copy-Item -Path $filepath -Destination "$dllsfilepath" }
}

$build="$root\build"; $dist="$root\dist";   $include="$root\Include"
$bin="$root\Scripts"; $pyenv="$root\pyenv.cfg"; $lib="$root\Lib";
$distr = @($build,$dist,$include,$bin,$pyenv)

foreach ($item in $distr){ if (Test-Path $item){ Remove-Item $item -Recurse -Force } }

if (Test-Path $lib)
{
    $input = Read-Host -Prompt 'Found old library files. Want a fresh library installation? [y/N] (defalt: N)'
    if (($input) -like "y") 
    {
        Write-Output "Removing old library files ..."
        Remove-Item $lib -Recurse -Force
    } 
    else { Write-Output "Keeping old library files ..." }
}

Invoke-Expression "$pybin $venvtarget ."

while (-not(Test-Path "$root\Scripts\activate")) {
    Write-Output "Wating for activation scripts to be created"
    Start-Sleep 1
}

if (Get-Command "conda.exe" -errorAction SilentlyContinue){ Invoke-Expression "conda deactivate" }
Invoke-Expression "$bin\activate"
Invoke-Expression "$bin\activate.bat"
Invoke-Expression "$bin\activate.ps1"
Invoke-Expression "$bin\python.exe -m pip install -U pip"
Invoke-Expression "$bin\python.exe -m pip install -r $root\requirements\dev.txt"
Invoke-Expression "$bin\python.exe setup.py bdist_wheel"
Invoke-Expression "$bin\python.exe  -m pip install -e $root"